                    Project Euler

What is it:
-----------
-This repository contains my solutions to some of the Project Euler problems.
 The approach is usually either bruteforce with small optimisations for worse-
 case scenarios or trying some "clever" solutions and comparing them.

To run:
-------
-Compile each file as a standalone program and run. The name of the file 
 represents what problem it tries to solve.

About missing problems:
-----------------------
-As it can be seen, there are some gaps in the continuity of the problems. Most
 of those have additional requirements (like downloading and reading external
 files or dealing with strings) which I could not be bothered to implement the 
 goal of this repository is to implement my solution/s to the more 
 "mathematical" problems and optimize them, not to deal with other nuances. for
 that kind of implementations see other repositories.
