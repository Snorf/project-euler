#include <stdio.h>
#include <string.h>
#include <time.h>

int multiply(char digits[], int multiple, int length)
{
    char multiple_digits[3];
    int multiple_length = 0;
    while (multiple > 0)
    {
        multiple_digits[multiple_length] = multiple%10;
        multiple /= 10;
        multiple_length++;
    }

    char sum_1[length+1];
    char sum_2[length+1];
    char sum_3[length+1];

    char *sum[3] = {sum_1, sum_2, sum_3};

    memset(sum_1, 0, length+2);
    memset(sum_2, 0, length+2);
    memset(sum_3, 0, length+2);

    int original_length = length;
    for (int i = 0; i < multiple_length; i++)
    {
        // printf("digits:");
        for (int j = 0; j < original_length; j++)
        {
            sum[i][j] += multiple_digits[i]*digits[j];
            // printf(" %d-%d", sum[i][j],digits[i]);
            while (sum[i][j] >= 10)
            {
                sum[i][j] -= 10;
                sum[i][j+1] ++;
                // printf(" %d", sum[i][j]);
                if (j+1 >= length) length++;
            } 
        }
        // printf("\t");
    }

    memset(digits, 0, 250);

    for (int i = 0; i < multiple_length; i++)
    {
        for (int j = 0; j < length; j++)
        {
            digits[j+i] += sum[i][j];
            while (digits[j+i] >= 10)
            {
                digits[j+i] -= 10;
                digits[j+i+1] ++;
                if (j+i+1 >= length) length++;
            } 
        }
    }
    // for (int j = 0; j < multiple_length; j++)
    // {
    //     printf("Sum: %d Digits:",j);
    //     for (int i = length; i >= 0; i--)
    //     {
    //         printf(" %d", sum[j][i]);
    //     }
    //     printf("\t");
    // }
    

    // printf("Digits2:");
    // for (int i = length; i >= 0; i--)
    // {
    //     printf(" %d", digits[i]);
    // }
    // printf("\n\n");

    return length+1;

}


void main(void)
{
    clock_t begin = clock();

    char digits[250];

    memset(digits, 0, 250);
    int length = 1;
    digits[0] = 1;
    // multiply(digits, 40, length);
    for (int i = 1; i < 101; i++)
        length = multiply(digits, i, length);

    printf("Digits:");
    int sum = 0;
    for (int i = length; i >= 0; i--)
    {
        printf(" %d", digits[i]);
        sum += digits[i];
    }
    printf("\n");
    printf("Result: %d\n", sum);

}