#include <stdio.h>
#include <time.h>
#include <stdint.h>


int get_chain(uint64_t number)
{
    int count = 0;
    //while loop seems to be, on average, slightly faster than for loop
    while (number > 1)
    {
        if (number%2) number = 3*number + 1;
        else number /= 2;
        count ++;
    }
    
    // for (count = 0; number > 1; count++)
    // {
    //     if (number%2) number = 3*number + 1;
    //     else number /= 2;
    // }
    return ++count;
}

void main(void)
{
    clock_t begin = clock();
    int current_max, current_max_count;
    // for loop wrapper to test timings
    for (int t = 0; t < 40; t++)
    {
        current_max = 0;
        current_max_count = 0;
        for (uint64_t i = 1; i < 1000000; i++)
        {
            //If an even number is not twice the current double, it means
            //the full chain except the first number has already been checked
            //this reduces runtime by ~35%
            if (!(i%2) && i/2 != current_max) continue;
            int current = get_chain(i);
            if (current > current_max_count)
            {
                current_max = i;
                current_max_count = current;
            }
        }
    }
    clock_t end = clock();
    double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
    printf("Current_max: %d \tchain_lenght: %d\n", current_max, current_max_count);
    printf("Time spent: %lf seconds\n", time_spent);
}