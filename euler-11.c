#include <stdio.h>



void main(void)
{
    int numbers[20][20];
    //llegir el fitxer

    int max = 0;
    int temp;
    //Check wether this or going around 3 times the full grid
    //one in each direction with skips is faster (similar to euler 8)
    for (int i = 0; i < 16; i++)
    {
        for (int j = 0; j < 16; j++)
        {
            temp = 1;
            //Horizontal
            for (int k = 0; k < 4; k++)
            {
                if (numbers[i][j+k] < 10) break;
                temp *= numbers[i][j+k];
            }
            if (temp > max) max = temp;

            temp = 1;
            //Vertical
            for (int k = 0; k < 4; k++)
            {
                if (numbers[i+k][j] < 10) break;
                temp *= numbers[i+k][j];
            }
            if (temp > max) max = temp;

            temp = 1;
            //Diagonal
            for (int k = 0; k < 4; k++)
            {
                if (numbers[i+k][j+k] < 10) break;
                temp *= numbers[i+k][j+k];
            }
            if (temp > max) max = temp;
        }
    }

    printf("Max: %d\n", max);
}