#include <stdio.h>
#include <time.h>

void main(void)
{
    clock_t begin = clock();

    int months1[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int months2[12] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    int curr_day = 6; //we want sundays, the first sunday is the 6th
    int curr_month = 1; //starting month, january
    int curr_year = 1901; //starting year, 1900

    int count = 0;

    while (curr_year < 2001)
    {
        //we check if the year is a leap year
        int months[12];
        if (!(curr_year%4) && ((curr_year%100) || !(curr_year%400)))
            for (int i = 0; i < 12; i++) months[i] = months2[i];
        else for (int i = 0; i < 12; i++) months[i] = months1[i];
        
        while (curr_month < 13)
        {
            while (curr_day < months[curr_month-1])
            {
                curr_day += 7;
                if (curr_day == 1) count++;
            }

            curr_day -= months[curr_month-1];
            curr_month++;
            if (curr_day == 1) count++;
        }
        curr_month = 1;
        curr_year++;
    }

    clock_t end = clock();
    double time_spent = (double) (end - begin)/CLOCKS_PER_SEC;

    printf("Number of Mondays: %d\t Time spent: %lf\n", count, time_spent);
}