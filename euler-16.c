#include <stdio.h>
#include <time.h>
#include <stdint.h>
#include <stdlib.h>

int get_sum(uint64_t mult)
{
    int count = 0;
    while (mult > 10)
    {
        count += mult%10;
        mult /= 10;
    }
    count += mult;
    return count;
}

int check_carry(char *nums, int count)
{
    for (int i = 0; i < count; i++)
    {
        if (nums[i] >= 10)
        {
            nums[i] -= 10;
            ++nums[i+1];
        }
    }
    if (nums[count] > 0) return ++count;
    return count;
}

void print_num(char *nums, int count)
{
    printf("current_num: ");
    for (int i = count - 1; i >= 0; i--)
    {
        printf("%d ", nums[i]);
    }
    printf("\t");
}

int sum_pot(int exp)
{
    //quite memory intensive, we can safely divide by 2 the current exponent without issues
    //abount memory bounds, but by 3 we could start having problems (ie 10/3 = 3, while 1024 needs 4
    //memory positions)
    unsigned char *nums = calloc((exp/3), sizeof(char));
    nums[0] = 1;
    int count = 1;
    int sum = 0;
    for (int k = 1; k <= exp; k++)
    {
        //this can only be done up to 2^3, because (2^4)^2=256 which is the size of uchar
        //which brings overflow issues
        if (exp - k >= 3)
        {
            for (int j = 0; j < count; j ++)
            nums[j] *= 8;
            k += 2;
        }
        else if (exp - k >= 2)
        {
            for (int j = 0; j < count; j ++)
                nums[j] *= 4; 
            k++;
        }
        else
            for (int j = 0; j < count; j ++)
            nums[j] *= 2; 
            

        for (int i = 0; i < count; i++)
        {
            while (nums[i] >= 10)
            {
                nums[i] -= 10;
                ++nums[i+1];
            }
        }
        if (nums[count] > 0) ++count;
        
        //count = check_carry(nums, count);
    }

    //print_num(nums, count);
    for (int i = 0; i < count; i++)
        sum += nums[i];

    free(nums);
    return sum;

}


//to aproach the overflow: keep an array that multiplies the numbers 1 by 1
//check performance?
void main(void)
{

    clock_t begin = clock();
    uint64_t mult = 1;
    //if done this way, the number overflows at 2^63
    /*for (int i = 1; i <= 1000; i++)
    {
        mult *= 2;
        int sum = get_sum(mult);
        printf("potencia: %d\tmult: %llu\tsum: %d\n", i, mult, sum);
    }*/

    int pot = sum_pot(100000);
    //wrapper to check time
    /*for (int i = 0; i < 1000; i++)
    {
        pot = sum_pot(1000000);
    }*/
    clock_t end = clock();
    double time_spent = (double)(end - begin)/CLOCKS_PER_SEC;
    printf("suma de la potencia: %d en %lf segons\n", pot, time_spent);

    
}