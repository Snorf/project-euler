#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#define MAX(a,b) (((a)>(b))?(a):(b))
#define MIN(a,b) (((a)<(b))?(a):(b))

int get_product()
{
    int a, b, c;
    for (int i = 500; i > 0; i--)
    {
        for (int j = 2; j < 500; j++)
        {
            for (int k = 2; k < 500; k++)
            {
                if ((i+j+k) != 1000) continue;
                if (((i*i + j*j) == k*k) || ((k*k + j*j) == i*i) ||
                    ((i*i + k*k) == j*j)) return i*j*k;
                //printf("a: %d, b: %d, c: %d\n", a, b, c);
            }
        }
    }
    return 0;
}

void main(void)
{
    //pure bruteforce
    //clock_t begin = clock();
    int sol = get_product();
    //clock_t end = clock();
    //double time_spent = (double) (end-begin) / CLOCKS_PER_SEC;
    printf("Solution: %d\n", sol);
    //printf("Time spent: %lf\n", time_spent);
}
    //Pythagorean triplets are 3 even numbers or 2 odd + 1 even
    // To fint pythagorean triplets:
    // a = n2 - m2
    // b = 2nm
    // c = n2 + m2
    // This will generate all primitive triplets, to find all of them
    // multiply each number by K
    // unsigned int a, b, c, a2 = 0, b2 = 0, c2 = 0;
    // unsigned int n = 2;
    // unsigned int m = 1;

    /*while((a + b + c) < 1000)
    {
        a = n*n - m*m;
        b = 2*n*m;
        c = n*n + m*m;
        /*for (int i = 2; ((a2 + b2 + c2) < 1000); i ++)
        {
            a2 = a*i;
            b2 = b*i;
            c2 = c*i;
        }
        printf("a: %d, b: %d, c: %d\tn: %d m: %d\n", a, b, c, n, m);
        a = a2;
        b = b2;
        c = c2;
        n++;
        m++;
    }*/

