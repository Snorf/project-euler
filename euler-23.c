#include <stdio.h>
#include <stdlib.h>

//notes:
//quick research shows that the smallest abundant number not divisible by 2 or
//3 is 5391411025, which is far greater that our max of 28123
//also, every multiple of a perfect number is abundant, as well as any multiple
//of an abundant number

//theorem: al even numbers greater than 46 are the sum of two abundant numbers
//in at least one way. ->sources oeis.org/wiki/Abuntant_numbers

//the first odd abundant number is 945
//with this and the previous theorem, any odd number between 3 and 945 cannot 
//be written as the sum of two abundant numbers (if all previous abundant 
//numbers are even, their sum will always be even)



//every integer greater than 20161 can be written as the sum of two abundant 
//numbers, so between that and our hard-cap we can sum all numbers

//a memory intensive solution would be to store all numbers that can be 
//expressed as the sum of two abundant numbers, generating them until the
//maximum surpasses the hard cap and then creating the sum from them

unsigned int get_sum_divisors(int num)
{
    int i;
    int max = num/2;
    unsigned int sum = 0;
    for (i = 1; i <= max; i++)
        if (!(num%i)) sum += i;
    return sum;
}

char is_abundant(int num)
{
    if (num%2 && num%3) return 0;
    return (get_sum_divisors(num) > num);
}

char is_abundant_sum_even(int num, int *abundant_even, int even_abs)
{
    int i, j;
    for (i = 0; i < even_abs; i++)
        for (j = 0; j < even_abs; j++)
            if (abundant_even[i] + abundant_even[j] == num) return 1;
    return 0;
}

char is_abundant_sum(int num, int *abundant_even, int even_abs, int *abundant_odd, int odd_abs)
{
    int i, j;
    for (i = 0; i < even_abs; i++)
        for (j = 0; j < odd_abs; j++)
            if (abundant_even[i] + abundant_odd[j] == num) return 1;
    return 0;
}

void main(void)
{
    int sum = 0;
    //all integers greater than 20161 don't have to be checked
    int max_int = 28123;
    int *abundant_even = calloc(10000, sizeof(int));
    int even_abs = 0;
    int *abundant_odd = calloc(10000, sizeof(int));
    int odd_abs = 0;

    int i = 0;
    
    for (i = 1; i < 24; i++)
    {
        sum += i;
        if (is_abundant(i))
        {
            abundant_even[even_abs] = i;
            even_abs++;
        }
    }

    //first we check even numbers lower than 47, and greater than 24
    //as 24 is the smallest sum of abundant numbers
    for (i = 24; i < 48; i+=2)
    {
        if (!is_abundant_sum_even(i, abundant_even, even_abs)) sum += i;
        if (is_abundant(i))
        {
            abundant_even[even_abs] = i;
            even_abs++;
        }
    }

    for (i = 25; i < 945; i += 2)
    {
        sum += i;
    }

    for (i = 48; i < 945; i += 2)
    {
        if (is_abundant(i))
        {
            abundant_even[even_abs] = i;
            even_abs++;
        }
    }
    for (i = 945; i < max_int; i++)
    {
        if (!(i%2))
        {
            if (is_abundant(i))
            {
                abundant_even[even_abs] = i;
                even_abs++;
            }
        }
        else
        {
            if (!is_abundant_sum(i, abundant_even, even_abs, abundant_odd, odd_abs)) sum += i;
            if (is_abundant(i))
            {
                abundant_odd[odd_abs] = i;
                odd_abs++;
            }

        }
    }

    printf("Final sum: %d\n", sum);
    free(abundant_even);
    free(abundant_odd);
}
