#include <stdio.h>
#include <time.h>

void main(void)
{
    //Looping through the set twice, but only stoping in the necessary ones
    //This is the fastest method of all three
    clock_t time1 = clock();
    int result = 0;
    for (int i = 0; i < 1000; i += 3)
        result += i;
    for (int i = 0; i < 1000; i += 5)
        if (i%3 != 0) result += i;
    clock_t time2 = clock();
    
    //Brute force, slowest method
    int result2 = 0;
    for (int i = 0; i < 1000; i++)
        if (i%3 == 0 || i%5 == 0) result2 += i;
    clock_t time3 = clock();

    //The problem iterates through a number of repetitive steps and it resets
    int steps[7] = {3, 2, 1, 3, 1, 2, 3};
    int offset = 0;
    int result3 = 0;
    int iterator = 0;
    while (iterator < 1000)
    {
        result3 += iterator;
        iterator += steps[offset];
        offset = (++offset > 6) ? 0 : offset;
    }
    clock_t time4 = clock();

    //An iteration of the fastest result. It's slightly slower, but still faster than the rest
    int result4 = 0;
    for (int i = 0; i < 1000; i += 3)
        result4 += i;
    for (int i = 0; i < 1000; i += 5)
        result4 += i;
    for (int i = 0; i < 1000; i += 15)
        result4 -= i;
    clock_t time5 = clock();
        

    printf("Result %d\n", result);
    printf("Result2 %d\n", result2);
    printf("Result3 %d\n", result3);
    printf("Result4 %d\n", result4);
    double time_spent1 = (double)(time2 - time1) / CLOCKS_PER_SEC;
    double time_spent2 = (double)(time3 - time2) / CLOCKS_PER_SEC;
    double time_spent3 = (double)(time4 - time3) / CLOCKS_PER_SEC;
    double time_spent4 = (double)(time5 - time4) / CLOCKS_PER_SEC;
    printf("Temps: %lf\n", time_spent1);
    printf("Temps2: %lf\n", time_spent2);
    printf("Temps3: %lf\n", time_spent3);
    printf("Temps4: %lf\n", time_spent4);
    
}