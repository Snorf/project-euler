#include <stdio.h>

//Euler's rule might not be accurate, so we bruteforce it

int get_divisor_sum(int original)
{
    int sum = 0;
    int max = original/2;
    for (int i = 1; i <= max; i ++)
    {
        if ((original%i) == 0) sum += i;
    }
    return sum;
}


void main(void)
{
    int sum = 0;
    //aproximation, won't be more than 10 pairs (20 elements) below 1000
    int pairs[20] = {0};
    char pair_count = 0;

    for (int i = 9999; i > 1; i--)
    {
        char checker = 0;
        for (int j = 0; j < pair_count*2; j++)
            if (pairs[j] == i) checker = 1;
        
        if (checker) continue;
        int candidate = get_divisor_sum(i);
        if (candidate == i) continue;
        if (candidate < 10000 && i == get_divisor_sum(candidate))
        {
            sum += candidate+i;
            pairs[pair_count*2] = i;
            pairs[pair_count*2+1] = candidate;
            pair_count++;
        }
    }

    printf("Final sum: %d\n", sum);
    

}