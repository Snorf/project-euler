#include <stdio.h>
#include <time.h>
#include <stdint.h>



//brute force
//calculates all permutations and counts the valid ones
//scales extremely badly, and takes >1 hour to compute (2^20 permutations to check)
uint64_t get_valid_permutations(int size, int depth, uint64_t permutation)
{
    uint64_t count = 0;
    if (depth > 0)
    {
        uint64_t current_perm = permutation;
        depth--;
        count += get_valid_permutations(size, depth, permutation);
        permutation |= 1 << depth;
        count += get_valid_permutations(size, depth, permutation); 
        
    }
    else
    {
        //There have to be 20 movements in one direction and 20 towards the other
        if (__builtin_popcountll(permutation) == size) count++;
    }

    return count;
}

//the formula for the total possible paths is, with size n:
//(2n)!/(n!*n!)
//to prevent overflow we divide at each step
//the formula comes from the calculation of all permutations with
//repetition. In this case, the divisor is (n!*n!) because there are
//only two movements (down or to the right) and both must be done n
//times each to reach the final point
uint64_t get_valid_permutations2(int size)
{
    uint64_t total = 1;
    for (uint64_t i = 1; i <= size; i++)
    {
        total *= i*(i+size);
        total /= i*i;
    }
    return total;
}

void main(void)
{
    clock_t begin = clock();
    uint64_t count = 0;
    int size = 20;

    //count += get_valid_permutations(size, size*2, 0);
    count += get_valid_permutations2(size);
    clock_t end = clock();
    double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
    printf("Number of valid permutations: %llu\n", count);
    printf("Is_same: %d\t", 137846528820 == count);
    printf("Time spent: %lf\n", time_spent);
}