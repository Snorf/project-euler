#include <stdio.h>
#include <time.h>

void main(void)
{
    //brute force
    clock_t begin = clock();
    unsigned long square_sum = 0;
    unsigned long sum_square = 0;
    
    //for loop wrapper to compare times
    for (int j = 0; j < 10000000; j ++)
    {
        //square_sum = (100*101) >> 1;  //this seems slower than adding in the for loop, 
                                      //but faster than manually dividing
        square_sum = 0;
        sum_square = 0;
        for (int i = 1; i < 101; ++i)
        {
            square_sum += i;
            sum_square += i*i;
        }
        square_sum *= square_sum;
    }
    unsigned long long final = square_sum - sum_square;
    clock_t end = clock();
    double time_spent = (double) (end-begin)/CLOCKS_PER_SEC;
    printf("Total : %d\n", final);
    printf("Time spent : %lf seconds\n", time_spent);
}
