#include <stdio.h>
#include <time.h>
#include <math.h>
#include <inttypes.h>
#include <stdlib.h>


// pure bruteforce, takes ~17 mins on my pc
int get_divisors(uint64_t current_triangular)
{
    uint64_t max = (uint64_t) current_triangular / 2 ;
    //count starts at 1 because every number can be divided by itself
    int count = 1;
    for (uint64_t i = 1; i <= max; i ++)
    {
        if ((current_triangular%i) == 0) count ++;
    }
    return count;
}

// Calculates the number of divisors following:
// if a number can be factorized into n = a^x*b^y*c^z
// where a, b and c are primes, the number of divisors of
// n is phi = (x+1)*(y+1)*(z+1)
// takes around ~0.185 seconds
int get_divisors2(uint64_t current_triangular)
{
    int current_count = 0;
    int current_div = 2;
    int count = 0;
    //experimental amount of memory, with 501 takes ~0.01 second longer
    int *divisors = calloc(50, sizeof(int));
    uint64_t max = (uint64_t)current_triangular/2;
    while(current_triangular > 1 && current_div < (max + 1))
    {
        if (!(current_triangular%current_div))
        {
            current_count++;
            current_triangular /= current_div;
        }
        else
        {
            if (current_count > 0)
            {
                divisors[count] = current_count;
                count++;
            }
            current_count = 0;
            current_div++;
        }
    }
    //if the last number we check is a divisor, it wouldn't be put into the list
    //so we make sure it is there
    if (current_count > 0)
    {
        divisors[count] = current_count;
        count++;
    }
    //We apply the formula
    int total = 1; 
    for (int i = 0; i < count; i++)
        total *= 1+divisors[i];
    //printf("Divisors: %d\t", total);
    return total;
}

void main(void)
{
    uint64_t current_triangular = 3;
    clock_t begin = clock();
    char found = 0;
    //some heuristics for divisor checks should make it faster
    for (int i = 3; !found; i++)
    {
        current_triangular += i;
        if (get_divisors2(current_triangular) > 500) break;
    }
    clock_t end = clock();
    double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
    printf("Time spent: %lf seconds\t", time_spent);
    printf("Max: %u\n", current_triangular);
    //printf("Max: %.0lf\n", current_triangular);
    
}