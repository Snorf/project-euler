#include <stdio.h>
#include <time.h>
#include <math.h>

typedef unsigned long long ulong;


char is_prime(ulong number)
{
    //TODO: previous primality testing, should be faster on big N
    //either fermat or miller-rabin (may be slower on small N)
    //It assumes that the number is greater than 5 and it's odd

    ulong max = (ulong) sqrt(number);
    //edge cases
    if (number%max == 0) return 0;

    for (int i = 5; i < max; i+=6)
        if (!(number%(i)) || !(number%(i+2))) return 0;
    return 1;//(number%2 || number%3);
}

void main(void)
{
    clock_t begin = clock();
    double sum = 5;
    double aux;
    //iterating in steps of 6 from 5 saves us multiple operations
    //(multiplication and one addition)
    for (ulong i = 5; i < 2000000; i+=6)
    {
        aux = sum;
        if (is_prime(i)) sum += i;
        if (is_prime(i+2) && i+2 < 2000000) sum += i+2;
        if (aux > sum) printf("Warning!\n");
    }
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Total sum: %.0lf\t", sum);
    printf("Is_prime: %d\t", is_prime(21+2));
    printf("Time spent: %lf secodns\n", time_spent);
}