#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>

void main(void)
{
    clock_t begin = clock();
    double max = 0;
    double temp, aux;
    //for loop wrapper to compare times
    for (int k = 0; k<10000; k++)
    {
        char string[1000] = "7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450";
        //uint64_t overflows
        
        //Convert to standard char
        for (int i = 0; i < 1000; i++)
            string[i] -= '0';
        for (int i = 0; i < 988; i++)
        {
            temp = 1;
            for (int j = 0; j < 13; j++)
            {
                //If there's a 0, the product will be 0 and we can skip it
                //If there's a 1, the product will be much lower, so we can skip it too
                //The 1 is purely experimental, as there could potentially be only sequences of 13
                //that contain 1's. If we remove it the time improvement it's just to 1/3rd, otherwise
                //reduces time to ~1/5th
                if (string[i+j] < 2)
                {
                    i = i+j;
                    break;
                }
                temp *= string[i+j];
                
            }
            //faster than ternary operator or "standard" MAX macro
            if (temp > max)
                max = temp;
        }
    }
    clock_t end = clock();
    double time_spent = (double) (end-begin)/CLOCKS_PER_SEC;
    printf("max: %.0lf\n", max);
    printf("time spent: %lf seconds\n", time_spent);
    
}