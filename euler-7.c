#include <stdio.h>
#include <time.h>
#include <math.h>

typedef unsigned long long ulong;


char is_prime(ulong number)
{
    //TODO: previous primality testing, should be faster on big N
    //either fermat or miller-rabin (may be slower on small N)

    ulong max = (ulong) sqrt(number);
    //edge cases
    if (number%max == 0) return 0;

    for (int i = 5; i < max; i+=6)
        if (!(number%(i)) || !(number%(i+2))) return 0;
    
    return (number%2 || number%3 || number%5 || number%11);
}

void main(void)
{
    clock_t begin = clock();
    ulong current_prime = 3;

    //for loop wrapper to compare times
    for (int j = 0; j < 1000; j++)
    {
        int count = 2;
        //iterating in steps of 6 from 5 saves us multiple operations
        //(multiplication and one addition)
        for (ulong i = 5; count < 10001; i+=6)
        {
            if (is_prime(i))
            {
                ++count;
                current_prime = i;
                if (count > 10000) break;
            }

            if (is_prime(i+2))
            {
                ++count;
                current_prime = i+2;
            }
        }
    }
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Current prime: %d\t", current_prime);
    printf("Time spent: %lf secodns\n", time_spent);
}
