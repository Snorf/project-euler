#include <stdio.h>
#include <time.h>

//the external for's are to measure performance
void main(void)
{
    clock_t time1 = clock();
    int a = 1;
    int b = 1;
    int c = 0;
    int result = 0;
    //Brute force
    for (int i = 0; i < 100000000; i ++)
    {
        while (c < 4000000)
        {
            if (!(c%2)) result += c;
            c = a + b;
            a = b;
            b = c;
        }
    }
    clock_t time2 = clock();
    int previous = 0;
    int actual = 2;
    int temp = 0;
    int result2 = 0;
    //"clever" solution, it's on par speed-wise
    for (int i = 0; i < 100000000; i++)
    {

        while (actual < 4000000)
        {
            result2 += actual;
            temp = actual;
            actual = (actual<<2) + previous;
            previous = temp;
        }
    }
    clock_t time3 = clock();
    
    double time_spent1 = (double)(time2 - time1) / CLOCKS_PER_SEC;
    double time_spent2 = (double)(time3 - time2) / CLOCKS_PER_SEC;
    printf("Result: %d\n", result);
    printf("Result2: %d\n", result2);
    printf("Temps: %lf\n", time_spent1);
    printf("Temps2: %lf\n", time_spent2);
}