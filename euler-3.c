#include <stdio.h>
#include <time.h>
#include <math.h>

typedef unsigned long long ulong;

/*char is_prime(ulong number)
{
    ulong max_div = sqrt(number);
    if (!(max_div%2)) --max_div;
    while(max_div > 1)
    {
        if (number%max_div == 0)
            return 0;
        max_div -= 2;
    }
    return 1;
}*/

//Improved version carried from problem 7
char is_prime(ulong number)
{
    //TODO: previous primality testing, should be faster on big N
    //either fermat or miller-rabin (may be slower on small N)

    ulong max = (ulong) sqrt(number);
    //edge cases
    if (number%max == 0) return 0;

    for (int i = 5; i < max; i+=6)
        if (!(number%(i)) || !(number%(i+2))) return 0;
    
    return (number%2 || number%3 || number%5 || number%11);
}

void main(void)
{
    clock_t begin = clock();
    ulong number = 600851475143;
    ulong min_num = sqrt(number);
    if (!(min_num%2)) --min_num;
    while(min_num > 0)
    {
        if (is_prime(min_num) && !(number%min_num)) break;
        min_num -= 2;
    }
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("min_num: %d\n", min_num);
    printf("time spent: %lf\n", time_spent);

}