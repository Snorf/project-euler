#include <stdio.h>
#include <time.h>

void main(void)
{
    //brute force with some improvements
    clock_t begin = clock();
    int i;

    //for loop wrapper to compare times
    for (int j = 0; j < 100000; j ++)
    {
        //It has to be a multiple of 20, 19 and 17 (biggest number and biggest primes), while also
        //being multiple of 16 and 18 (so 20*19*17*13*11*7*4)
        //therefore it has to be a multiple of 25865840
        for (i = 25865840; ; i+=25865840)
        {
            //it's not necessary to check all conditions as some imply others
            if (!(i%9))
                break;
        }
    }

    //To be fair, the result is 20*19*17*11*13*7*9*4 as all of them combined imply the result
    //being divisible by the rest of numbers (it's divisible by 2 because it's multiple of 20,
    //18 because 20 implies 2 and it is multiple of 9, 16 because 20 implies 4 and it's also 
    //multiplied by 4, 15 because 20 implies 5 and 9 implies 3, and so on and so forth).
    
    //Basically it is only needed to check that the multipliers are either coprime or that the
    //common divisors squared are another divisor inside the range (ie: 20 and 4 have 4 as common
    //divisor, but 16 is in the list).
    
    clock_t end = clock();
    double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
    printf("Resultat: %d en: %lf segons\n", i, time_spent);
}